from django.contrib import admin

from .models import *

admin.site.register(Gift)
admin.site.register(Connection)
admin.site.register(ConnectionRequest)
admin.site.register(Claim)
