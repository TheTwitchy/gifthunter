from .models import *

from django.db.models.query_utils import Q

def get_connections(user_id):
    return Connection.objects.all().filter(Q(giftee__id = user_id) | Q(gifter__id = user_id))

def search_gh_users(search_term, user_id):
    users = User.objects.all().filter(Q(first_name__icontains=search_term) | Q(last_name__icontains=search_term) | Q(username__icontains=search_term) | Q(email__icontains=search_term)).exclude(pk = user_id)

    # Remove people with whom connections already exist, so when searching you will only see unconnected users
    current_connections = get_connections(user_id)
    for connection in current_connections:
        users = users.exclude(pk = connection.giftee.id)
        users = users.exclude(pk = connection.gifter.id)
    return users