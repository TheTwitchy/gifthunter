from django.db import models
from django.contrib.auth.models import User

class Gift(models.Model):
    owner = models.ForeignKey(User, verbose_name="Owner")
    name = models.CharField(max_length=255, verbose_name="Name")
    link = models.URLField(max_length=2048, verbose_name="Link", blank=True, null=True)
    description = models.TextField(verbose_name="Description", blank=True, null=True)
    created_at = models.DateField(auto_now=False, auto_now_add=True)
    #updated_at = models.DateField(auto_now=True, auto_now_add=False)
    priority = models.CharField(max_length=20, verbose_name="Priority", choices=(("high", "High"),("medium", "Medium"),("low", "Low")), default="medium")
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def can_view(self, request):
        if request.user == self.owner:
            return True
        # See if these users are connected
        owner_obj = GifthunterUser.objects.filter(id = self.owner.id)[0]
        if owner_obj.is_connected(request):
            return True

        return False

    def can_edit(self, request):
        if not self.can_view(request):
            return False
        if request.user == self.owner:
            return True
        return False

class GifthunterUser(User):
    class Meta:
        proxy = True

    def is_connected(self, request):
        if request.user == self:
            return True
        
        # Look for connections between the two people. Any conection will allow the people to access basic information about each other.  This is ugly and makes me want to cry, but it's late and I want a beer.
        if Connection.objects.all().filter(giftee_id = request.user.id, gifter_id = self.id):
            return True
        if Connection.objects.all().filter(gifter_id = request.user.id, giftee_id = self.id):
            return True

        return False

# A connection is basically "friending" another user, you can see gift wishlists for each other. It used to be one way, hence the names.
class Connection(models.Model):
    giftee = models.ForeignKey(User, verbose_name = "Giftee", related_name = "giftee")
    gifter = models.ForeignKey(User, verbose_name = "Gifter", related_name = "gifter")

    def __str__(self):
        return str(self.giftee) + " connected to " + str(self.gifter)

class ConnectionRequest(models.Model):
    from_user = models.ForeignKey(User, verbose_name = "from_user", related_name = "from_user")
    to_user = models.ForeignKey(User, verbose_name = "to_user", related_name = "to_user")

    def __str__(self):
        return str(self.from_user) + " requesting to connect to " + str(self.to_user)

# A claim is someone "claiming" a gift for another person, in that they intend to get the person that gift. There's no record of the person it's for, as the gift itself will contain that information. The giftee will be informed and have the gift changed to 'non_active' the day of the "reveal_on" date.
class Claim(models.Model):
    owner = models.ForeignKey(User, verbose_name="Owner")
    gift = models.ForeignKey(Gift, verbose_name = "Gift")

    def __str__(self):
        return str(self.owner) + " claiming " + self.gift.name + " for " + str(self.gift.owner)

