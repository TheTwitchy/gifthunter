from django import shortcuts,urls,template,http
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserChangeForm

from .models import *
from .forms import *
from .helpers import *

@login_required
def index(request):
    context = {}
    page_template = template.loader.get_template('main/index.html')

    return http.HttpResponse(page_template.render(context, request))

def setup(request):
    context = {}
    page_template = template.loader.get_template('main/setup.html')

    if len(GifthunterUser.objects.all()) == 0:
        user = GifthunterUser(username="admin", is_superuser = True, is_staff = True)
        user.set_password("admin")
        user.save()
        context["msg"] = "Admin setup, login with admin:admin. You'll want to change the password on the 'Administration' page as well, otherwise someone else will login to this instance."
    else:
        context["msg"] = "Already setup."

    return http.HttpResponse(page_template.render(context, request))

@login_required
def list_gifts(request, owner_id):
    context = {}
    page_template = template.loader.get_template('main/list_gifts.html')

    # Retreve all Gift objects owned by user
    gift_objs = None
    owner_obj = None

    try:
        gift_objs = Gift.objects.all().filter(owner_id = owner_id, active = True)
        owner_obj = GifthunterUser.objects.all().filter(pk = owner_id)[0]
        context["msg"] = request.session.pop("msg", "")
        context["msg_type"] = request.session.pop("msg_type", "")
    except:
        raise http.Http404("Could not retrieve objects.")

    context["owner"] = owner_obj
    context["gifts"] = gift_objs

    return http.HttpResponse(page_template.render(context, request))

@login_required
def view_gift(request, gift_id):
    context = {}
    page_template = template.loader.get_template('main/view_gift.html')

    # Retreve the Gift objects if owned or viewable by current user
    gift_obj = shortcuts.get_object_or_404(Gift, pk = gift_id)

    context["gift"] = gift_obj
    context["msg"] = request.session.pop("msg", "")
    context["msg_type"] = request.session.pop("msg_type", "")
    
    if request.method == "GET":
        pass
    elif request.method == "POST":
        # This part is used exclusively for claiming gifts. Dunno how well this will work.
        
        # Ensure a claim doesn't already exist
        claim_obj = Claim.objects.filter(gift__id = gift_id)
        if claim_obj:
            context["msg"] = "This gift has already been claimed."
            context["msg_type"] = "error"
        else:
            claim_obj = Claim(gift = gift_obj, owner = request.user)
            claim_obj.save()
            context["msg"] = "Gift claimed successfully."
            context["msg_type"] = "success"
    else:
        raise http.Http500("Unknown HTTP method.")
    return http.HttpResponse(page_template.render(context, request))

@login_required
def edit_gift(request, gift_id):
    context = {}
    page_template = template.loader.get_template('main/edit_gift.html')

    # Retreve the Gift objects if owned or viewable by current user
    gift_obj = shortcuts.get_object_or_404(Gift, pk = gift_id)

    if gift_obj.can_edit(request):
        context["gift"] = gift_obj
    else:
        raise http.Http404("Gift not found.")

    if request.method == "GET":
        context["form"] = GiftForm(instance=gift_obj)

    elif request.method == "POST":
        context["form"] = form = GiftForm(request.POST, instance=gift_obj)
        if form.is_valid():
            #This validates the form
            form.save()
            request.session["msg"] = gift_obj.name + " updated successfully"
            request.session["msg_type"] = "success"
            return shortcuts.redirect("main:view_gift", gift_id = gift_obj.id)

        else:
            context["msg"] = "Failed to update Gift"
            context["msg_type"] = "error"
    else:
        raise http.Http500("Unknown HTTP method.")

    return http.HttpResponse(page_template.render(context, request))

@login_required
def new_gift(request):
    context = {}
    page_template = template.loader.get_template('main/edit_gift.html')

    if request.method == "GET":
        context["form"] = GiftForm()

    elif request.method == "POST":
        context["form"] = form = GiftForm(request.POST)
        if form.is_valid():
            # We don't want to commit right away, as we need to set the owner. In theory this will always be the current user.
            gift_obj = form.save(commit = False)
            gift_obj.owner = request.user
            gift_obj.save()

            request.session["msg"] = gift_obj.name + " created successfully"
            request.session["msg_type"] = "success"
            return shortcuts.redirect("main:view_gift", gift_id = gift_obj.id)

        else:
            context["msg"] = "Failed to create new gift"
            context["msg_type"] = "error"
    else:
        raise http.Http500("Unknown HTTP method.")

    return http.HttpResponse(page_template.render(context, request))


@login_required
def remove_gift(request, gift_id):
    context = {}

    # Retreve the Gift objects if owned or viewable by current user
    gift_obj = shortcuts.get_object_or_404(Gift, pk = gift_id)

    if gift_obj.can_edit(request):
        context["gift"] = gift_obj
    else:
        raise http.Http404("Gift not found.")

    if request.method == "GET":
        gift_obj.delete()
        request.session["msg"] = gift_obj.name + " removed successfully"
        request.session["msg_type"] = "success"
        return shortcuts.redirect("main:list_gifts", owner_id = request.user.id)
    else:
        raise http.Http500("Unknown HTTP method.")


# This was originally intended to be a way to view any user, but was reduced to just seeing yourself.
@login_required
def view_user(request):
    context = {}
    page_template = template.loader.get_template('main/view_user.html')

    if request.method == "GET":
        context["form"] = GifthunterUserForm(instance=request.user)

    elif request.method == "POST":
        context["form"] = form = GifthunterUserForm(request.POST, instance=request.user)
        if form.is_valid():
            #This validates the form
            form.save()
            context["msg"] = "Account updated successfully"
            context["msg_type"] = "success"

        else:
            context["msg"] = "Failed to update account"
            context["msg_type"] = "error"
    else:
        raise http.Http500("Unknown HTTP method.")

    return http.HttpResponse(page_template.render(context, request))

@login_required
def view_connections(request):
    context = {}
    page_template = template.loader.get_template('main/view_connections.html')

    context["msg"] = request.session.pop("msg", "")
    context["msg_type"] = request.session.pop("msg_type", "")

    # Retreve all users
    context["users"] = user_objs = User.objects.all().exclude(pk = request.user.id)

    # Retrieve all ConnectionRequests where this user is in the "to_user" field
    context["pending_connections"] = ConnectionRequest.objects.filter(to_user__id = request.user.id)

    return http.HttpResponse(page_template.render(context, request))

@login_required
def search_users(request):
    context = {}
    page_template = template.loader.get_template('main/search_users.html')

    if request.method == "GET":
        context["form"] = form = UserSearchForm()

    elif request.method == "POST":
        context["form"] = form = UserSearchForm(request.POST)

        if form.is_valid():
            # Retreve all connections owned by the current user
            context["user_results"] = user_objs = search_gh_users(form.cleaned_data["search_param"], request.user.id)
            context["is_search"] = True
        else:
            context["msg"] = "Error while searching"
            context["msg_type"] = "error"
    else:
        raise http.Http500("Unknown HTTP method.")

    return http.HttpResponse(page_template.render(context, request))

@login_required
def connection_request(request, user_id):
    context = {}
    page_template = template.loader.get_template('main/connection_request.html')

    # Check to see if these users are already connected, and if so redirect to their list.
    user_obj = shortcuts.get_object_or_404(GifthunterUser, pk = user_id)
    if user_obj.is_connected(request):
        return shortcuts.redirect("main:list_gifts", owner_id = user_id)

    context["to_user"] = user_obj

    if request.method == "GET":
        pass
        # We actually don't need to do anything special here.

    elif request.method == "POST":
        # Check to see if there's already a connection request out
        req_obj = ConnectionRequest.objects.filter(to_user__id = user_id, from_user__id = request.user.id)
        if req_obj:
            # There's already a request out.
            context["msg"] = "Connection request has already been sent"
            context["msg_type"] = "error"
        else:
            connection_request_obj = ConnectionRequest(to_user = user_obj, from_user = request.user)
            connection_request_obj.save()

            context["msg"] = "Sent connection request"
            context["msg_type"] = "success"
            
    else:
        raise http.Http500("Unknown HTTP method.")

    return http.HttpResponse(page_template.render(context, request))
    

@login_required
def pending_connection(request, connection_request_id):
    context = {}
    page_template = template.loader.get_template('main/pending_connection.html')

    # Make sure IDOR isn't an issue here... Although technically the error returned by a non-existent CR is not the same as one that exists, but is for different users, so you could in theory enumerate CR ids. Probably not a huge issue unless there's another way to view CRs, but that's something else entirely.
    connection_request_obj = shortcuts.get_object_or_404(ConnectionRequest, pk = connection_request_id)
    if not connection_request_obj.to_user == request.user:
        raise http.Http404("Not found.")

    context["from_user"] = connection_request_obj.from_user

    if request.method == "GET":
        pass
        # We actually don't need to do anything special here.

    elif request.method == "POST":
        if request.POST["action"] == "accept":
            # Create new connection for the users. First mae sure the connection doesn't exist. No idea how this would happen, but better safe than stupid.
            
            from_user_obj = shortcuts.get_object_or_404(GifthunterUser, pk = connection_request_obj.from_user.id)
            
            if from_user_obj.is_connected(request):
                context["msg"] = "These users are already connected"
                context["msg_type"] = "error"
            else:
                connection_obj = Connection(giftee = from_user_obj, gifter = request.user)
                connection_obj.save()

                connection_request_obj.delete()
                request.session["msg"] = "Accepted connection request"
                request.session["msg_type"] = "success"

                return shortcuts.redirect("main:view_connections")

        elif request.POST["action"] == "decline":
            # Just delete the connection request
            connection_request_obj.delete()
            request.session["msg"] = "Deleted connection request"
            request.session["msg_type"] = "success"

            

        else:
            context["msg"] = "Unknown action"
            context["msg_type"] = "error"

            
    else:
        raise http.Http500("Unknown HTTP method.")

    return http.HttpResponse(page_template.render(context, request))

@login_required
def list_claims(request):
    context = {}
    page_template = template.loader.get_template('main/list_claims.html')

    # Retreve all claim objects owned by user
    context["claims"] = claim_objs = Claim.objects.all().filter(owner_id = request.user.id)

    context["msg"] = request.session.pop("msg", "")
    context["msg_type"] = request.session.pop("msg_type", "")

    return http.HttpResponse(page_template.render(context, request))

@login_required
def view_claim(request, claim_id):
    context = {}
    page_template = template.loader.get_template('main/view_claim.html')

    # Retreve the Gift objects if owned or viewable by current user
    claim_obj = shortcuts.get_object_or_404(Claim, pk = claim_id)
    if not claim_obj.owner == request.user:
        raise Http404("Not found.")

    context["claim"] = claim_obj
    if request.method == "GET":
        pass
    if request.method == "POST":
        if request.POST["action"] == "release":
            claim_obj.delete()
            request.session["msg"] = "Claim released successfully"
            request.session["msg_type"] = "success"
            return shortcuts.redirect("main:list_claims")
        elif request.POST["action"] == "finalize":
            
            gift_obj = shortcuts.get_object_or_404(Gift, pk = claim_obj.gift.id)
            gift_obj.delete()
            claim_obj.delete()
            request.session["msg"] = "Claim finalized successfully"
            request.session["msg_type"] = "success"
            return shortcuts.redirect("main:list_claims")
        else:
            context["msg"] = "Unknown action"
            context["msg_type"] = "error"

    return http.HttpResponse(page_template.render(context, request))




def handler404(request):
    response = shortcuts.render_to_response('404.html', {}, context_instance = template.RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    response = shortcuts.render_to_response('500.html', {}, context_instance = template.RequestContext(request))
    response.status_code = 500
    return response
