from django import forms

from .models import *

class GiftForm(forms.ModelForm):
    class Meta:
        model = Gift
        fields = ["name", "priority", "link", "description"]

class GifthunterUserForm(forms.ModelForm):
    class Meta:
        model = GifthunterUser
        fields = ["first_name", "last_name"]

class UserSearchForm(forms.Form):
    search_param = forms.CharField(label = "Search", max_length = 256)